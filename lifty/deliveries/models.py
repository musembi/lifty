from django.db import models
from django.urls import reverse

UNITS_OF_MEASURE = (
    ('kg', 'Kilogram'),
    ('t', 'Tonne'),
    ('bags', 'Bag')
)


class Delivery(models.Model):

    delivered_on = models.DateTimeField()
    quantity = models.DecimalField(max_digits=15, decimal_places=2)
    item = models.CharField(max_length=255)
    unit = models.CharField(max_length=255, choices=UNITS_OF_MEASURE)
