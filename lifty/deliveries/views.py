from django.views.generic import CreateView, UpdateView, ListView

from .forms import DeliveryForm
from .models import Delivery


class DeliveryCreateView(CreateView):

    form_class = DeliveryForm
    model = Delivery
    success_url = '/deliveries/list/'


class DeliveryListView(ListView):

    model = Delivery
    ordering = ['-delivered_on',]

