from django import forms

from .models import Delivery


class DateTimeInput(forms.DateTimeInput):
    input_type = 'datetime-local'

    def __init__(self, **kwargs):
        kwargs['format'] = '%Y-%m-%dT%H:%M'
        super().__init__(**kwargs)


class DeliveryForm(forms.ModelForm):

    # delivered_on = forms.DateTimeInput(widget=DateTimeInput)

    class Meta:
        model = Delivery
        fields = ('delivered_on', 'item', 'quantity', 'unit',)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['delivered_on'].widget = DateTimeInput()
        self.fields['delivered_on'].input_formats = ['%Y-%m-%dT%H:%M', '%Y-%m-%d %H:%M']
