from django.urls import path
from django.contrib import admin

from .views import DeliveryCreateView, DeliveryListView

urlpatterns = [
    path('create/', DeliveryCreateView.as_view(), name='delivery_create'),
    path('list/', DeliveryListView.as_view(), name='delivery_list'),
]
